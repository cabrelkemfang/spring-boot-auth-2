package com.example.spring_security.service;

import com.example.spring_security.model.Role;
import com.example.spring_security.model.User;
import com.example.spring_security.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
@Transactional
@Service
public class CustumUserService implements UserDetailsService {

  @Autowired
  private UserRepository userRepository;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    User user = userRepository.findByUsername(username);
    if (user == null) {
      throw new UsernameNotFoundException("Invalid username or password.");
    }

    Set<GrantedAuthority> grantedAuthorities = getAuthorities(user);

    return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), grantedAuthorities);
  }


  private Set<GrantedAuthority> getAuthorities(User user) {
    Set<Role> roleByUserId = user.getRoles();
    final Set<GrantedAuthority> authorities = roleByUserId.stream().map(
            role -> new SimpleGrantedAuthority("ROLE_" + role.getName().toString().toUpperCase()))
            .collect(Collectors.toSet());
    return authorities;
  }
}

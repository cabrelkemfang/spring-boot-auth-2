package com.example.spring_security.controller;


import com.example.spring_security.model.User;
import com.example.spring_security.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

  private final UserRepository userRepository;
  private final BCryptPasswordEncoder passwordEncoder;

  @Autowired
  public UserController(final UserRepository userRepository,
                        final BCryptPasswordEncoder passwordEncoder) {
    super();
    this.userRepository = userRepository;
    this.passwordEncoder = passwordEncoder;
  }

  @Secured({"ROLE_USER"})
  @RequestMapping(value = "/user", method = RequestMethod.GET)
  public ResponseEntity<List<User> > listUser() {
    List<User> userList;
    userList = this.userRepository.findAll();
    return new ResponseEntity<>(userList, HttpStatus.OK);
  }

  @RequestMapping(value = "/user", method = RequestMethod.POST)
  public ResponseEntity<User> create (@RequestBody User user) {
    User user1 = new User();
    user1.setPassword(this.passwordEncoder.encode(user.getPassword()));
    user1.setUsername(user.getUsername());
    user1.setId(user.getId());
    user1.setAge(user.getAge());
    user1.setSalary(user.getSalary());
    user1.setRoles(user.getRoles());
    User user2 = this.userRepository.save(user1);
    return new ResponseEntity<>(user2, HttpStatus.CREATED);
  }

  @RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
  public void delete(@PathVariable(value = "id") Long id) {
    this.userRepository.delete(id);
  }

}

